#if ! defined (SYMTABLE_H)
#define SYMTABLE_H 1

#include <stdbool.h>
#include "symbole.h"
#include "ABR.h"
#include "stats.h"

/**
 * @file
 * @brief Les types nécessaires à la manipulation de la table
 * des symboles d'un exécutable, c'est-à-dire la sortie du linker.
 * @author F. Boulier
 * @date novembre 2010
 */

/***********************************************************************
 * IMPLANTATION
 ***********************************************************************/

/**
 * @struct symtable
 * @brief Le type \p struct \p symtable fournit une implantation très 
 * rudimentaire de la table des symboles d'un exécutable. Les
 * données sont stockées dans un arbre binaire de recherche \p arbre.
 */

struct symtable
{   struct ABR* arbre;    /**< l'arbre */
    struct stats mesures; /**< les mesures */
};

/**
 * @enum symbole_recherche
 * @brief Le type \p enum \p symbole_recherche permet d'indiquer aux itérateurs
 * de type \p struct \p iterateur_symtable, le type de symbole
 * recherché. Les valeurs possibles sont \p symbole_indefini,
 * \p symbole_duplique et \p symbole_quelconque.
 */

enum symbole_recherche 
		{ symbole_indefini, symbole_duplique, symbole_quelconque };

/**
 * @struct iterateur_symtable
 * @brief Le type \p struct \p iterateur_symtable fournit un itérateur
 * de symboles, dans un exécutable. Le champ \p table pointe vers
 * l'exécutable à parcourir. 
 * Le champ \p critere donne le critère de recherche.
 * Le champ \p pile contient une pile d'arbres binaires de recherche.
 * Si ABR est présent dans la pile, alors sont sous-arbre gauche est
 * déjà parcouru. Reste à considérer sa valeur, puis celle de son
 * sous-arbre droit.
 */

struct iterateur_symtable
{   struct pile_ABR pile;     /**< la pile des arbres à considérer */
    struct symtable* table;  /**< l'exécutable à parcourir */
    enum symbole_recherche critere; /**< le critère de recherche */
};

/***********************************************************************
 * PROTOTYPES DES FONCTIONS (TYPE ABSTRAIT)
 ***********************************************************************/

extern void init_symtable (struct symtable*);
extern void clear_symtable (struct symtable*);
extern void enregistrer_dans_symtable (struct symtable*, struct symbole*);
extern struct symbole* rechercher_dans_symtable (char*, struct symtable*);
extern int synthese_symtable (struct symtable*);

extern struct symbole* first_symbole_symtable 
			(struct iterateur_symtable*, struct symtable*, 
			 enum symbole_recherche);
extern void clear_iterateur_symtable (struct iterateur_symtable*);
extern struct symbole* next_symbole_symtable (struct iterateur_symtable*);

#endif
