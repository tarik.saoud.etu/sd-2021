#if ! defined (SYMTABLE_H)
#define SYMTABLE_H 1

#include <stdbool.h>
#include "tableau.h"
#include "stats.h"

/**
 * @file
 * @brief Les types nécessaires à la manipulation de la table
 * des symboles d'un exécutable, c'est-à-dire la sortie du linker.
 * @author F. Boulier
 * @date novembre 2010
 */

/***********************************************************************
 * IMPLANTATION
 ***********************************************************************/

/**
 * @struct symtable
 * @brief Le type \p struct \p symtable fournit une implantation très 
 * rudimentaire de la table des symboles d'un exécutable. Les
 * données sont stockées dans un tableau redimensionnable.
 * Les symboles sont stockés en vrac.
 * effectivement utilisés (le nombre de symboles).
 */

struct symtable
{   struct tableau T;
    struct stats mesures; /**< mesures */
};

/***********************************************************************
 * PROTOTYPES DES FONCTIONS (TYPE ABSTRAIT)
 ***********************************************************************/

extern void init_symtable (struct symtable*);
extern void clear_symtable (struct symtable*);
extern void enregistrer_dans_symtable (struct symtable*, struct symbole*);
extern struct symbole* rechercher_dans_symtable (char*, struct symtable*);
extern int synthese_symtable (struct symtable*);

#endif
