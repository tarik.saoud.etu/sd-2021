#if ! defined (ERROR_H)
#define ERROR_H 1

/**
 * @file 
 * @brief Le prototype de la fonction \p error
 * @author F. Boulier
 * @date novembre 2010
 */

/***********************************************************************
 * PROTOTYPES DES FONCTIONS (TYPE ABSTRAIT)
 ***********************************************************************/

extern void error (char*, char*, int);

#endif
