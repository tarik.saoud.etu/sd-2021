#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "table_simple_hachage.h"
#include "error.h"

/**
 * @file
 * @brief Implantation d'une table de hachage de symboles 
 * avec résolution des collisions par listes chaînées.
 * @author F. Boulier
 * @date mars 2011
 */

static int fonction_simple_hachage_par_defaut
	(char* clef,
	 struct table_simple_hachage* table)
{   long p, i, h;

    p = table->N;

    h = 0;
    for (i = 0; clef [i] != '\0'; i++)
	h = (h + clef [i] * clef [i]) % p;
    return h;
}

static void init_alveole (struct alveole* a)
{
    init_liste_symbole (&a->L);
}

/**
 * @brief Constructeur. Initialise la table \p table avec au moins
 * \p N alvéoles et la fonction de hachage \p h. Si \p h est le pointeur
 * nul, une fonction de hachage par défaut est choisie.
 * @param[out] table une table de hachage
 * @param[in] N un nombre d'alvéoles souhaité
 * @param[in] h l'adresse d'une fonction de hachage
 */

void init_table_simple_hachage 
	(struct table_simple_hachage* table, int N, fonction_simple_hachage* h)
{   long i;

    if (h != (fonction_simple_hachage*)0)
	table->h = h;
    else
	table->h = &fonction_simple_hachage_par_defaut;

    table->N = N;
    table->tab = (struct alveole*)malloc 
				(table->N * sizeof (struct alveole));
    if (table->tab == (struct alveole*)0)
	error ("init_table_simple_hachage", __FILE__, __LINE__);
    for (i = 0; i < table->N; i++)
	init_alveole (&table->tab [i]);

    table->nb_non_vides = 0;
    table->nb_collisions = 0;
}

/**
 * @brief Destructeur. Libère les ressources consommées par \p table.
 * @param[in,out] table une table de hachage
 */

void clear_table_simple_hachage (struct table_simple_hachage* table)
{   int i;
    for (i = 0; i < table->N; i++)
	clear_liste_symbole (&table->tab [i].L);
    if (table->tab)
	free (table->tab);
}

/**
 * @brief Retourne le taux de remplissage de la table de hachage,
 * c'est-à-dire le rapport du nombre d'alvéoles non vides divisé
 * par le nombre d'alvéoles alloués.
 * @param[in] table une table de hachage
 */

double taux_simple_remplissage_table_simple_hachage 
					(struct table_simple_hachage* table)
{
    return (double)table->nb_non_vides / (double)table->N;
}

/**
 * @brief Recherche un symbole d'identificateur clef dans la table. 
 * Affecte l'adresse de ce symbole à \p *sym s'il existe, sinon zéro.
 * Retourne le nombre de comparaisons de chaînes de caractères effectuées.
 * @param[out] sym un pointeur sur un symbole
 * @param[in] clef une chaîne de caractères
 * @param[in,out] table une table de hachage
 */

int rechercher_symbole_dans_table_simple_hachage 
	(struct symbole** sym, char* clef, struct table_simple_hachage* table)
{   int hashval, nbcomp;

    hashval = (*table->h) (clef, table);
    nbcomp = rechercher_dans_liste_symbole (sym, clef, &table->tab [hashval].L);
    if (*sym)
	table->nb_collisions += nbcomp - 1;
    else
	table->nb_collisions += nbcomp;
    return nbcomp;
}
    
/**
 * @brief Ajoute sym dans la table. On suppose que le symbole n'appartient
 * pas à la table.
 * @param[in,out] table une table de hachage
 * @param[in] sym un symbole
 */

void ajouter_symbole_dans_table_simple_hachage 
		(struct table_simple_hachage* table, struct symbole* sym)
{
    int hashval;

    hashval = (*table->h) (sym->ident, table);
    if (est_vide_liste_symbole (&table->tab [hashval].L))
	table->nb_non_vides += 1;
    ajouter_en_tete_liste_symbole (&table->tab [hashval].L, sym);
}

