/* rationnel.c */

#include <stdio.h>
#include "rationnel.h"

/* retourne le pgcd de a0 et b0 */

static int Euclide (int a0, int b0)
{   int a, b, t;

    if (a0 > 0) a = a0; else a = - a0;
    if (b0 > 0) b = b0; else b = - b0;
    while (b != 0)
    {   t = a % b;   /* le reste de la division de a par b */
        a = b;
        b = t;
    }
    return a;
}

void init_rationnel (struct rationnel* R, int p0, int q0)
{   int p, q, g;

    g = Euclide (p0, q0);
    if (q0 > 0)
    {   p = p0 / g;
        q = q0 / g;
    } else
    {   p = - p0 / g;
        q = - q0 / g;
    }
    R->numer = p;
    R->denom = q;
}

void clear_rationnel (struct rationnel* R)
{
}

/* Bug ! */

void add_rationnel
    (struct rationnel* S, struct rationnel* A, struct rationnel* B)
{   int p, q;
    p = A->numer * B->denom + A->denom * B->numer;
    q = A->denom * B->denom;
    init_rationnel (S, p, q);
}

void print_rationnel (struct rationnel R)
{
    if (R.denom == 1)
        printf ("%d\n", R.numer);
    else
        printf ("%d/%d\n", R.numer, R.denom);
}

