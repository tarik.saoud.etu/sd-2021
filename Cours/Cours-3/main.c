/* main.c */

#include "liste.h"

int main ()
{   double tab[] = { 178, -5, 39 };
    int n = sizeof(tab)/sizeof(double);

    struct liste L0;

    init_liste (&L0); // L0 = la liste vide
    for (int i = 0; i < n; i++)
    {   ajout_en_tete_liste (&L0, tab[i]); // ajoute tab[i] en tête de L0
        print_liste (L0);
    }
    clear_liste (&L0);
    return 0;
}

